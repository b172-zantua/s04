--a. find all artists that has letter D in its name

--full_name nakalagay sakin sir
SELECT * FROM artists WHERE full_name LIKE "%D%";


--b. find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;


--c. join albums and songs table. only show the album name, song name, length

SELECT albums.album_title, songs.song_name, songs.length FROM songs
	JOIN albums ON songs.album_id = albums.id;


--d. join the artists and albums tables. find all albums that has a letter a in its name

SELECT * FROM albums
	JOIN artists ON albums.artist_id = artists.id
    WHERE album_title LIKE "%A%";


--e. sort the albums in Z-A order. show only the first 4 records

SELECT * FROM albums ORDER BY album_title DESC LIMIT 0,4;


--f. join the albums and songs tables.(sort album from Z-A and sort songs from A-Z)

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC;
